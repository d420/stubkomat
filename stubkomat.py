from time import time, sleep
import os
from decimal import Decimal
from itertools import cycle
from hashlib import sha256
from random import choice

from flask import make_response
from flask import Blueprint
from flask_restful import Resource, Api
from flask_sockets import Sockets

from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
import marshmallow
from marshmallow import validate
import simplejson

class Socketry:

    registered_sockets = set()
    handled_sockets = set()
    started_at = None
    mode = None

    def qrcode_start(self, mode, force_break):
        if mode not in ['token', 'address']:
            raise ValueError
        self.force_break = force_break
        self.mode = mode
        self.started_at = time()
        self.handled_sockets = set()

    def qrcode_trigger(self, socket):
        # imaginary broadcast
        triggered = self.started_at and time() - self.started_at > 3
        if not triggered:
            self.registered_sockets.add(socket)
        s_ok = socket in self.registered_sockets and socket not in self.handled_sockets
        return triggered and s_ok

    def socket_data(self):

        if self.force_break:
            return {
                'type': self.mode,
                'errors': ['invalid_qrcode']
            }

        if self.mode == 'address':
            return simplejson.dumps({
                'type': 'address',
                'data': {'address': '1GoGoHuBvLP9rgf8NHgarwf2Cn9ef6QR3k'}
            })

        if self.mode == 'token':
            return simplejson.dumps({
                'type': 'token',
                'data': {'token': '1234'}
            })

        raise ValueError()


socketry = Socketry()

class APIException(Exception):
    @property
    def data(self):
        return {'errors': [self.error]}


class upexc:
    class NoTransactionFound(APIException):
        """ token/email/phonenumber coudn't find any transaction to redeem """
        error = 'transaction_not_found'

        rollback = True

    class ExpiredTransaction(APIException):
        """ active transaction has expired """
        error = 'transaction_expired'

        rollback = True

    class UnconfirmedTransaction(APIException):
        """ can't redeem transaction yet
            since the user doesn't get the token until it's confirmed this
            exception is pointless :^)

        """
        error = 'transaction_not_confirmed'

    class SpentTransaction(APIException):
        """ transaction alread redeemed """
        error = 'transaction_spent'

        rollback = True

    class ExpiredTicker(APIException):
        """ exchange rate doesn't update
            happens only in inactive state, since the ticker is frozen for active transactions
        """
        error = 'expired_ticker'

        state_break = True

    class InvalidAddress(APIException):
        error = 'invalid_address'

    class InvalidContact(APIException):

        error = 'contact_error'

    class InvalidEmail(InvalidContact):
        error = 'contact_email_error'

    class InvalidPhone(InvalidContact):
        error = 'contact_phone_error'

    class ConnectionError(APIException):
        """ internet is dead. """
        error = 'uplink_error'

        state_break = True
        rollback = True


class atmexc:
    class ATMStateError(APIException):
        """ Frontend is trying to do an undefined state transiition :^) """
        error = 'atm_state_failure'

        state_break = True
        rollback = True

    class ATMActive(APIException):
        """ Frontend is trying to start a transaction while there's one already active.
            Frontend should know better.
        """
        error = 'atm_active'

    class ATMFailure(APIException):
        """  """
        error = 'atm_failure'

        state_break = True
        rollback = True


class TX:
    def __init__(self, type_, cash_currency, crypto_currency, destination=None, cash_amount=None, crypto_amount=None):
        self.type_ = type_
        self.cash_currency = cash_currency
        self.crypto_currency = crypto_currency
        self.destination_wallet = destination
        self.created = int(time())
        self.type_ = type_
        self.phone = None
        self.email = None
        self.crypto_amount = Decimal('0.001')
        self._cash_amount = None
        self.disbursing = 0
        self.rollback = False
        if cash_amount:
            self.cash_amount = cash_amount

    @property
    def notes_in(self):
        notes_count = (int(time()) - self.created) // 5
        return [50] * notes_count

    @property
    def cash_amount(self):
        return self._cash_amount or sum(self.notes_in)

    @cash_amount.setter
    def cash_amount(self, value):
        self._cash_amount = value

    @property
    def state(self):
        return 'selling' if self.type_ == 'sell' else 'buying'

    def disburse(self):
        self.disbursing = time() + 8


buy_tx_errors = cycle([upexc.ExpiredTransaction, upexc.InvalidAddress])
sell_tx_errors = cycle([upexc.ExpiredTransaction])
redeem_tx_errors = cycle([upexc.SpentTransaction, upexc.UnconfirmedTransaction])


class ATM:
    tx = None

    def __init__(self):
        self.broken = False
        self.atm_state = 'inactive'
        self.sell_tx = None
        self.force_break = bool(os.getenv('break', False))
        self.force_no_uplink = bool(os.getenv('breakuplink', False))

    @property
    def active_tx(self):
        return self.tx and (self.tx.disbursing == 0 or time() < self.tx.disbursing)

    @property
    def session_state(self):
        tx = self.tx

        if self.broken:
            session_state = self.last_error
        elif tx and time() < self.tx.disbursing:
            session_state = 'rollbacking' if self.tx.rollback else 'disbursing'

        elif self.force_no_uplink:
            session_state = 'uplink_error'

        else:
            session_state = tx.state if tx else self.atm_state

        data = {
            'session_state': session_state,
            'broken': self.broken or self.force_no_uplink,
            'notes_in': tx.notes_in if tx else [],
            'crypto_amount': tx.crypto_amount if tx else 0,
            'cash_amount': tx.cash_amount if tx else 0,
            'crypto_currency': tx.crypto_currency if tx else '',
            'cash_currency': tx.cash_currency if tx else '',
            'destination_wallet': tx.destination_wallet if tx else ''
        }
        return data

    def buy_start(self, cash_currency, crypto_currency, destination):
        if self.active_tx:
            raise atmexc.ATMActive()
        self.tx = TX('buy', cash_currency, crypto_currency, destination=destination)

    def buy_finalize(self):
        if not self.active_tx:
            raise atmexc.ATMStateError()
        if self.force_break:
            raise next(buy_tx_errors)

        data = {
            'tx_hash': sha256(str(id(self.tx)).encode()).hexdigest(),  # blockchain hash
            'tx_id': id(self.tx),  # internal id
            'crypto_currency': self.tx.crypto_currency,
        }
        self.tx = None
        return data

    def buy_rollback(self):
        if not self.active_tx:
            raise atmexc.ATMStateError()
        self.tx.disburse()
        self.tx.rollback = True

    def sell_start(self, cash_currency, crypto_currency):
        if self.active_tx:
            raise atmexc.ATMActive()
        self.tx = TX('sell', cash_currency, crypto_currency)

    def sell_finalize(self, cash_amount, email, phone):
        if not self.active_tx:
            raise atmexc.ATMStateError()
        if self.force_break:
            raise next(sell_tx_errors)

        if (self.tx.email, self.tx.phone) == (None, None):
            raise atmexc.ATMStateError()  #  this shouldn't happen yo

        if self.tx.email != email or self.tx.phone != phone:
            raise atmexc.ATMStateError()  #  this shouldn't happen either yo

        crypto_amount = Decimal('0.0001')
        self.tx.cash_amount = cash_amount
        self.tx.crypto_amount = crypto_amount

        address = '1GoGoHuBvLP9rgf8NHgarwf2Cn9ef6QR3k'

        data = {
            'tx_id': id(self.tx),
            'target_address': address,
            'payment_uri': 'bitcoin:{}?amount={}'.format(address, crypto_amount),
            'crypto_amount': crypto_amount
        }
        self.sell_tx, self.tx = self.tx, None
        return data

    def sell_cancel(self):
        self.tx = None

    def sell_auth_contact(self, phone, email):
        if not self.active_tx:
            raise atmexc.ATMStateError()
        if phone:

            if self.force_break:
                raise upexc.InvalidPhone()

            self.tx.phone = phone
            print("{} gets a nice text message".format(phone))
        if email:

            if self.force_break:
                raise upexc.InvalidEmail()

            self.tx.email = email
            print("{} gets a nice email".format(email))

    def sell_redeem(self, email, phone, token, token_qrcode):
        if token_qrcode and token_qrcode != '1234:':
            raise upexc.NoTransactionFound()

        if token and not (phone or email):
            raise ValueError(phone, email)

        if token and token != '1337':
            raise upexc.NoTransactionFound()

        if self.force_break:
            raise next(redeem_tx_errors)
        self.tx = self.sell_tx or TX('sell', 'PLN', 'BTC', cash_amount=Decimal('300'))
        self.tx.disburse()
        return {
            'cash_amount': self.tx.cash_amount,
            'cash_currency': self.tx.cash_currency,
            'crypto_amount': self.tx.crypto_amount,
            'crypto_currency': self.tx.crypto_currency,
        }

    def get_exchange_rates(self, cash_currency, crypto_currency):

        trend = choice(['rising', 'falling', 'constant'])
        return {
            'exchange_sell_rate': Decimal('18000'),
            'exchange_sell_trend': trend,
            'exchange_buy_rate': Decimal('22000'),
            'exchange_buy_trend': trend,
        }

    def cash_to_crypto(self, amount):
        if not self.active_tx:
            raise atmexc.ATMStateError("This should not happen")
        rates = self.get_exchange_rates(self.tx.cash_currency, self.tx.crypto_currency)

        return {
            'crypto_amount_sell': amount / rates['exchange_sell_rate'],
            'crypto_amount_buy':  amount / rates['exchange_buy_rate'],
        }


atm_manager = ATM()


blueprint = Blueprint('api', __name__)
blueprint_ws = Blueprint('ws', __name__)


api = Api(blueprint, errors={
    'NoTransactionFound': {'status': 404},
    'InvalidBuyRequest': {'status': 500},
    'ExpiredTransaction': {'status': 500},
    'SpentTransaction': {'status': 500},
    'ExpiredTicker': {'status': 500},
    'NoTickerException': {'status': 500},
    'InvalidAddress': {'status': 500},
    'ATMActive': {'status': 500},
    'ATMFailure': {'status': 500},
    'ATMStateError': {'status': 500},
})


@api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(simplejson.dumps(data), code)
    resp.headers.extend(headers or {})
    return resp


cash_to_crypto_args = {
    'cash_amount': fields.Decimal(required=True),
}

exchange_rate_args = {
    'crypto_currency': fields.String(required=True),
    'cash_currency': fields.String(required=True),
}


@api.resource('/cash_to_crypto', endpoint='cash_to_crypto')
class CashToCrypto(Resource):
    @use_kwargs(cash_to_crypto_args)
    def get(self, cash_amount):
        """

        :param string cash_amount':

        :>json crypto_amount_sell: crypto
        :>json crypto_amount_buy: crypto

        """
        return atm_manager.cash_to_crypto(cash_amount)


@api.resource('/crypto_exchange_rate', endpoint='crypto_exchange_rate')
class CryptoExchangeRate(Resource):
    @use_kwargs(exchange_rate_args)
    def get(self, cash_currency, crypto_currency):
        """
        :param string cash_currency: [PLN]
        :param string crypto_currency: [BTC]

        :>json string exchange_sell_rate: fiat
        :>json string exchange_buy_rate: fiat
        """

        return atm_manager.get_exchange_rates(cash_currency, crypto_currency)


@api.resource('/session_state', endpoint='session_state')
class SessionState(Resource):
    def get(self):
        """
            here be dragons

            :>json string session_state: [inactive, buying, selling, disbursing, error codes]
            :>json bool broken:
            :>json int notes_in:
            :>json string crypto_amount:
            :>json string cash_amount:
            :>json string crypto_currency:
            :>json string cash_currency:
            :>json string destination_wallet:
        """
        return atm_manager.session_state


buying_session_args = {
    'cash_currency': fields.Str(required=True),
    'crypto_currency': fields.Str(required=True),
    'destination_wallet': fields.Str(required=True),
}

selling_session_args = {
    'cash_currency': fields.Str(required=True),
    'crypto_currency': fields.Str(required=True),
}


trigger_qrreader_args = {
    'mode': fields.Str(required=True, validators=[validate.OneOf(['address', 'token'])]),
}


@api.resource('/trigger_qrreader', endpoint='trigger_qrreader')
class TrigerQRReader(Resource):

    @use_kwargs(trigger_qrreader_args)
    def post(self, mode):
        socketry.qrcode_start(mode, atm_manager.force_break)

        return {'success': True}


@api.resource('/buy/start_session', endpoint='start_buying_session')
class BuyStartSession(Resource):

    @use_args(buying_session_args)
    def post(self, args):
        """
        :form string cash_currency: [PLN]
        :form string crypto_currency: [BTC]
        :form string destination_wallet: crypto address
        """

        atm_manager.buy_start(
            args['cash_currency'],
            args['crypto_currency'],
            args['destination_wallet'])

        return {'success': True}


@api.resource('/buy/accept_session', endpoint='accept_buying_session')
class BuyFinalizeSession(Resource):
    def post(self):
        """

        :>json string tx_hash': blockchain hash
        :>json string tx_id': upstream id

        """
        return atm_manager.buy_finalize()


@api.resource('/buy/rollback_session', endpoint='rollback_buying_session')
class BuyRollbackSession(Resource):
    def post(self):
        atm_manager.buy_rollback()
        return {'success': True}


@api.resource('/sell/start_session', endpoint='start_selling_session')
class StartSellingSession(Resource):

    @use_kwargs(selling_session_args)
    def post(self, cash_currency, crypto_currency):
        """
            :form string cash_currency: [PLN]
            :form string crypto_currency: [BTC]
        """

        atm_manager.sell_start(
            cash_currency,
            crypto_currency
        )
        return {'success': True}


class ContactArgs(marshmallow.Schema):
    strict = True
    email = fields.Str(validate=validate.Email)
    phone = fields.Str(validate=validate.ContainsOnly('0123456789'))

    @marshmallow.validates_schema
    def validate_schema(self, data):
        if 'email' not in data and 'phone' not in data:
            raise marshmallow.ValidationError("Phone or email not provided",
                                              ['email', 'phone'])


class FinalizeSellingArgs(marshmallow.Schema):
    strict = True
    cash_amount = marshmallow.fields.Decimal(required=True)
    email = fields.Str(validate=validate.Email)
    phone = fields.Str(validate=validate.ContainsOnly('0123456789'))

    @marshmallow.validates_schema
    def validate_schema(self, data):
        if 'email' not in data and 'phone' not in data:
            raise marshmallow.ValidationError("Phone or email not provided",
                                              ['email', 'phone'])


@api.resource('/sell/accept_session', endpoint='finalize_selling_session')
class FinalizeSellingSession(Resource):
    @use_args(FinalizeSellingArgs(strict=True))
    def post(self, args):
        """
        :form string cash_amount:
        :form string email:
        :form string phone:

        :>json string tx_id: upstream id
        :>json string target_address:
        :>json string payment_uri:
        :>json string crypto_amount:
        """
        return atm_manager.sell_finalize(
            args['cash_amount'],
            args.get('email', None),
            phone=args.get('phone', None))


@api.resource('/sell/rollback_session', endpoint='cancel_selling_session')
class CancelSellingSession(Resource):
    def post(self):
        atm_manager.cancel_selling_session()
        return {'success': True}


finalize_selling_args = {
    'email': fields.Str(),
    'phone': fields.Str(),
    'token': fields.Str(),
    'token_qrcode': fields.Str()
}

# ((email||phone) && token) || token_qrcode


@api.resource('/sell/auth/contact', endpoint='sell_ping')
class SendContactConfirmation(Resource):
    @use_kwargs(ContactArgs(strict=True))
    def post(self, phone, email):
        atm_manager.sell_auth_contact(phone, email)
        return {'success': True}


@api.resource('/sell/redeem', endpoint='redeem')
class RedeemSellingSession(Resource):
    @use_kwargs(finalize_selling_args)
    def post(self, email, phone, token, token_qrcode):
        return atm_manager.sell_redeem(email, phone, token, token_qrcode)




@blueprint_ws.route('/ws')
def echo_socket(socket):
    while not socket.closed:
        sleep(1)
        # imaginary broadcast yo
        if not socketry.qrcode_trigger(socket):
            continue


        socket.send(socketry.socket_data())
        socketry.handled_sockets.add(socket)


def set_cors_policy(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Credentials', True)
    return response


from flask import Flask
app = Flask(__name__)
sockets = Sockets(app)
app.after_request(set_cors_policy)
app.register_blueprint(blueprint, url_prefix='/api')
sockets.register_blueprint(blueprint_ws)

if __name__ == "__main__":
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(('', 5000), app, handler_class=WebSocketHandler)
    server.serve_forever()
