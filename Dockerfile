FROM ubuntu

RUN apt-get update && \
    apt-get -y install python3 \
                       python3-dev \
                       python3-pip \
                       locales

COPY requirements.txt app/requirements.txt
RUN pip3 install -r app/requirements.txt

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN useradd -d /app app
USER app
WORKDIR /app

EXPOSE 5000
ENV PYTHONUNBUFFERED 1
COPY stubkomat.py app/stubkomat.py
ENV FLASK_APP app/stubkomat.py

CMD gunicorn -k flask_sockets.worker stubkomat:app -b 0.0.0.0:5000
